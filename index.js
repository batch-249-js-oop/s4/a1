'use strict';

class Student {
    constructor(name,email,grades){
        this.name = name;
        this.email = email;
        this.passed = undefined;
        this.passWithHonors = undefined;
        this.gradeAve = undefined;
        if(grades.constructor===Array && grades.length===4){
            grades.every(grade => {
                return (grade >=0 && grade <= 100) ? this.grades = grades : this.grades = undefined;
            })
        } else {
            this.grades = undefined;
        }
    }

    login(){
        console.log(this.email, `has logged in`);
        return this;
    };
    logout(){
        console.log(this.email, `has logged out`);
        return this;
    };
    listGrades(){
        console.log(this.name, `quarterly grades are: `, this.grades);
        return this;
    };
    computeAve(){
        this.gradeAve = this.grades.reduce((previousGrade, currentGrade) =>{
            return previousGrade + currentGrade;
        })/(this.grades.length);
        
        return this;
    };
    willPass(){
        this.computeAve();
        if(this.gradeAve >= 85){
            this.passed = true;
        } else {
            this.passed = false;
        }
        return this;
    };
    willPassWithHonors(){
        this.willPass();
        if(this.passed){
            (this.gradeAve>=90)? this.passWithHonors = true : this.passWithHonors = false;
        }
        return this;
    };
}


//class to rerepsent a section of students
class Section {
    constructor(name){
        this.name = name;
        this.students = [];
        this.honorStudents = undefined;
        this.honorsPercentage = undefined;
        this.honorStudentInfo = [];
    }
    addStudent(name,email,grades){
        let student = new Student (name,email,grades);
        if (student.constructor === Student){
            this.students.push(student);
        }
        return this;
    }
    countHonorStudents(){
        this.honorStudents = this.students.filter(student => {
            student.willPassWithHonors()
            return student.passWithHonors;
        }).length;

        return this;
    }
    computeHonorsPercentage(){
        this.countHonorStudents();
        this.honorsPercentage = (this.honorStudents/this.students.length)*100 + '%';
        return this;
    }
    retrieveHonorStudentInfo(){
        this.students.forEach(student => {
            student.willPassWithHonors();
            if(student.passWithHonors){
                let honorStudent = {
                    name : student.name,
                    email: student.email,
                    gradeAve: student.gradeAve,
                }
                this.honorStudentInfo.push(honorStudent);
            }
        })
        return this;
    }
    sortHonorStudentsByGradesDesc(){
        this.retrieveHonorStudentInfo();
        this.honorStudentInfo.sort((previousHonorStudent,currentHonorStudent) => {
            return currentHonorStudent.gradeAve - previousHonorStudent.gradeAve;
        })
        return this;
    }
}

class Grade{
    constructor(level){
        if(typeof level === 'number'){
            this.level = level;
        }
        this.sections = [];
        this.totalStudents = 0;
        this.totalHonorStudents = 0;

        this.batchAveGrade = undefined;
        this.batchMinGrade = undefined;
        this.batchMaxGrade = undefined;
    }
    addSection(section){
        if(typeof section === 'string'){
            this.sections.push(new Section (section));
        }
        return this;
    }
    countStudents(){
        this.totalStudents = this.sections.map(section => {
            return section.students.length
        }).reduce((previousNumber,currentNumber)=>{
            return previousNumber + currentNumber
        })
        return this;
    }
    countHonorStudents(){
        this.totalHonorStudents = this.sections.map(section => {
            section.countHonorStudents();
            return section.honorStudents;
        }).reduce((previousNumber,currentNumber) =>{
            return previousNumber + currentNumber;
        })
        return this;
    }
    computeBatchAve(){
        this.countStudents();
        //Using map,flat and reduce
        this.batchAveGrade = this.sections.map(section => {
            return section.students.map(student => {
                student.computeAve();
                return student.gradeAve;
            })
        })
        .flat()
        .reduce((previousNumber,currentNumber)=>{
            return previousNumber + currentNumber;
        })/(this.totalStudents);

        //using ForEach
        // let count = 0;
        // this.sections.forEach(section =>{
        //     section.students.forEach(student => {
        //         student.grades.forEach(grade => {
        //             count +=grade;
        //         })
        //     })
        // })
        // this.batchAveGrade = count/(this.totalStudents*4);
        return this;
    }
    getBatchMinGrade(){
        let minBatchGrade = [];
        this.sections.forEach(section =>{
            section.students.forEach(student => {
                student.grades.forEach(grade => {
                    minBatchGrade.push(grade);
                })
            })
        })
        minBatchGrade.sort();
        this.batchMinGrade = minBatchGrade[0];
        return this;
    }
    getBatchMaxGrade(){
        let maxBatchGrade = [];
        this.sections.forEach(section =>{
            section.students.forEach(student => {
                student.grades.forEach(grade => {
                    maxBatchGrade.push(grade);
                })
            })
        })
        maxBatchGrade.sort();
        this.batchMaxGrade = maxBatchGrade[maxBatchGrade.length-1];
        return this;
    }
}

const grade1 = new Grade(1);
grade1.addSection("section1A");
grade1.sections.find(section => section.name === "section1A").addStudent('John', 'john@mail.com', [89, 84, 78, 88]);
grade1.sections.find(section => section.name === "section1A").addStudent('Joe', 'joe@mail.com', [78, 82, 79, 85]);
grade1.sections.find(section => section.name === "section1A").addStudent('Jane', 'jane@mail.com', [87, 89, 91, 93]);
grade1.sections.find(section => section.name === "section1A").addStudent('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);

grade1.addSection("section1B");
grade1.sections.find(section => section.name === "section1B").addStudent('Jeremy', 'jeremy@mail.com', [85, 82, 83, 89]);
grade1.sections.find(section => section.name === "section1B").addStudent('Johnny', 'johnny@mail.com', [82, 86, 77, 88]);
grade1.sections.find(section => section.name === "section1B").addStudent('Jerome', 'jerome@mail.com', [89, 85, 92, 91]);
grade1.sections.find(section => section.name === "section1B").addStudent('Janine', 'janine@mail.com', [90, 87, 94, 91]);

grade1.addSection("section1C");
grade1.sections.find(section => section.name === "section1C").addStudent('Faith', 'faith@mail.com', [87, 85, 88, 91]);
grade1.sections.find(section => section.name === "section1C").addStudent('Hope', 'hope@mail.com', [85, 87, 84, 89]);
grade1.sections.find(section => section.name === "section1C").addStudent('Love', 'love@mail.com', [91, 87, 90, 88]);
grade1.sections.find(section => section.name === "section1C").addStudent('Joy', 'joy@mail.com', [92, 86, 90, 89]);

grade1.addSection("section1D");
grade1.sections.find(section => section.name === "section1D").addStudent('Eddie', 'eddie@mail.com', [85, 87, 86, 92]);
grade1.sections.find(section => section.name === "section1D").addStudent('Ellen', 'ellen@mail.com', [88, 84, 86, 90]);
grade1.sections.find(section => section.name === "section1D").addStudent('Edgar', 'edgar@mail.com', [90, 89, 92, 86]);
grade1.sections.find(section => section.name === "section1D").addStudent('Eileen', 'eileen@mail.com', [90, 88, 93, 84]);
